import { defineConfig, splitVendorChunkPlugin } from 'vite';
import react from '@vitejs/plugin-react-swc';
import tsconfigPaths from 'vite-tsconfig-paths';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [tsconfigPaths(), react()],
  build: {
    rollupOptions: {
      output: {
        // manualChunks: (id) => {
        //   if (id.includes('node_modules')) {
        //     if (id.includes('@material-ui')) {
        //       return 'vendor_mui';
        //     } else if (id.includes('verbum')) {
        //       return 'vendor_verbum';
        //     } else if (id.includes('firebase')) {
        //       return 'vendor_firebase';
        //     } else if (id.includes('react')) {
        //       return 'vendor_react';
        //     } else if (id.includes('react-dom')) {
        //       return 'vendor_react-dom';
        //     }

        //     return 'vendor'; // all other package goes here
        //   }
        // }
        manualChunks: (id) => {
          if (id.includes('node_modules'))
            return id
              .toString()
              .split('node_modules/')[1]
              .split('/')[0]
              .toString();
        }
      }
    }
  }
});
