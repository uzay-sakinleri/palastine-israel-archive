import React from 'react';
import ReactDOM from 'react-dom/client';
import { RouterProvider } from 'react-router-dom';
import {
  CssBaseline,
  GlobalStyles,
  ThemeProvider,
  createTheme
} from '@mui/material';
import orange from '@mui/material/colors/orange';
import AuthProvider from './providers/AuthProvider';
import { router } from './Router';
import './main.css';

declare module '@mui/material/styles' {
  interface Palette {
    pRed: string;
    pBlack: string;
    pWhite: string;
    pGreen: string;
  }
  interface PaletteOptions extends Partial<Palette> {}
}

const palastineTheme = createTheme({
  palette: {
    pRed: '#EE2A35',
    pBlack: '#000',
    pWhite: '#FFF',
    pGreen: '#009736'
  },
  components: {
    MuiLink: {
      styleOverrides: {
        underlineNone: true
      }
    }
  }
});

// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
    <CssBaseline />
    <GlobalStyles
      styles={{
        a: {
          '&.active': { backgroundColor: orange[800] }
        }
      }}
    />
    <ThemeProvider theme={palastineTheme}>
      <AuthProvider>
        <RouterProvider router={router} />
      </AuthProvider>
    </ThemeProvider>
  </React.StrictMode>
);
