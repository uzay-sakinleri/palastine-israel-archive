export type Article = {
  id: string;
  userId: string;
  title: string;
  content: string;
};
