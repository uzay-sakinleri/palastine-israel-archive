import { useTheme } from '@emotion/react';
import { Box, Paper, Theme, Typography } from '@mui/material';
import Grid from '@mui/material/Grid';
import { QueryDocumentSnapshot } from 'firebase/firestore';
import { useLoaderData, useNavigate } from 'react-router-dom';

const Articles = () => {
  const theme = useTheme() as Theme;
  const navigate = useNavigate();
  const { docs } = useLoaderData() as {
    docs: QueryDocumentSnapshot[];
  };
  return (
    <Grid
      container
      spacing={{ xs: 2, md: 3 }}
      columns={{ xs: 4, sm: 8, md: 12 }}
    >
      {docs.map((doc) => {
        return (
          <Grid key={doc.id} item xs={2} md={4}>
            <Paper
              sx={{
                overflow: 'hidden',
                backgroundColor: theme.palette.pWhite + '66',
                '&:hover': {
                  backgroundColor: theme.palette.pGreen + 'CC',
                  cursor: 'pointer'
                }
              }}
              elevation={12}
              onClick={() => {
                navigate(`${doc.id}`);
              }}
            >
              <Typography
                sx={{
                  textAlign: 'center',
                  marginTop: '6px',
                  borderBottom: '2px solid black'
                }}
                component="h2"
                variant="h4"
              >
                {doc.get('title')}
              </Typography>
              <Box sx={{ padding: '16px' }}>{doc.get('content')}</Box>
            </Paper>
          </Grid>
        );
      })}
    </Grid>
  );
};

export default Articles;
