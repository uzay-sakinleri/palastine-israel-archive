import { createContext } from 'react';
import { AuthContextTypes } from 'src/types/firebaseTypes';

export const AuthContext = createContext<AuthContextTypes | null>(null);
