import { FirebaseApp } from 'firebase/app';
import { Auth, User } from 'firebase/auth';
import { Firestore } from 'firebase/firestore';

export type FirebaseTypes = {
  app: FirebaseApp;
  firestore: Firestore;
};

export type AuthContextTypes = {
  user: User | null;
  error: Error | undefined;
  auth: Auth | undefined;
};
