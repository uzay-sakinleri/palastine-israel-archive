import {
  Button,
  FormControl,
  Paper,
  TextField,
  Theme,
  Typography
} from '@mui/material';
import { useState } from 'react';
import { Form, useActionData } from 'react-router-dom';
import { useTheme } from '@emotion/react';
import loginAction from 'src/action-loaders/login/loginAction';

type Credentials = {
  email: string;
  password: string;
};

const Login = () => {
  const theme = useTheme() as Theme;
  const error = useActionData() as ReturnType<typeof loginAction>;
  if (error) console.error(error);
  const [loginFormData, setLoginFormData] = useState<Credentials>({
    email: '',
    password: ''
  });
  const [warning, setWarning] = useState<string>('');

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    if (loginFormData.email || loginFormData.password) setWarning('');
    setLoginFormData({
      ...loginFormData,
      [e.target.name]: e.target.value
    });
  };

  return (
    <>
      <Paper
        sx={{
          '& form': {
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            alignContent: 'center',
            gap: '20px',
            padding: '20px'
          },
          display: 'flex',
          maxWidth: '500px',
          margin: '50px auto',
          alignItems: 'center',
          justifyContent: 'center',
          flexDirection: 'column',
          padding: '70px',
          gap: '10px'
        }}
        elevation={12}
      >
        <Typography component="h3" variant="h3">
          Login
        </Typography>
        <Typography
          sx={{
            marginTop: '10px',
            color: theme.palette.pRed,
            lineBreak: 'auto'
          }}
          variant="body2"
          component="span"
        >
          {warning && warning}
        </Typography>
        <Form method="POST" action="/login">
          <FormControl variant="filled" component="fieldset">
            <TextField
              label="Email"
              type="email"
              value={loginFormData.email}
              autoComplete="true"
              required
              onChange={handleChange}
              name="email"
              sx={{ minWidth: '200px' }}
            />
          </FormControl>
          <FormControl>
            <TextField
              label="Password"
              type="password"
              value={loginFormData.password}
              required
              onChange={handleChange}
              name="password"
              autoComplete="true"
              sx={{ minWidth: '200px' }}
            />
          </FormControl>
          <Button
            type="submit"
            sx={{
              color: theme.palette.pGreen,
              backgroundColor: theme.palette.pGreen + '66',
              padding: '10px'
            }}
            onClick={() => {
              if (!loginFormData.email && !loginFormData.password) {
                setWarning(
                  'Lütfen gerekli alanları doldurun ve tekrar deneyin.'
                );
              }
            }}
          >
            Submit
          </Button>
        </Form>
      </Paper>
    </>
  );
};

export default Login;
