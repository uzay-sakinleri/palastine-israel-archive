import { useLoaderData } from 'react-router-dom';

type Article = {
  id: string;
  userId: string;
  title: string;
  content: string;
};

const SingleArticle = () => {
  const article = useLoaderData() as Article;
  return (
    <>
      <h1>{article.content}</h1>
    </>
  );
};

export default SingleArticle;
