import { FC, useEffect, useState } from 'react';
import {
  BoldButton,
  CodeFormatButton,
  FontFamilyDropdown,
  FontSizeDropdown,
  InsertDropdown,
  InsertLinkButton,
  ItalicButton,
  TextColorPicker,
  TextFormatDropdown,
  UnderlineButton,
  Divider,
  AlignDropdown,
  BackgroundColorPicker,
  Editor,
  EditorComposer,
  ToolbarPlugin
} from 'verbum';
import { fontFamilies, fontSizes } from '../constants/fontConstants';

const VerbumEditor: FC = (): JSX.Element => {
  const [editorState, setEditorState] = useState<string>('');
  useEffect(() => {
    if (editorState && import.meta.env.DEV)
      console.log(JSON.parse(editorState));
  }, [editorState]);

  return (
    <EditorComposer>
      <Editor
        hashtagsEnabled={true}
        actionsEnabled={true}
        autoLinkEnabled={true}
        emojisEnabled={true}
        placeholder="Hello!"
        onChange={(editorState: string) => {
          setEditorState(editorState);
        }}
      >
        <ToolbarPlugin defaultFontSize="20px" defaultFontFamily="Roboto">
          <FontFamilyDropdown fontOptions={fontFamilies} />
          <FontSizeDropdown fontSizeOptions={fontSizes} />
          <Divider />
          <BoldButton />
          <ItalicButton />
          <UnderlineButton />
          <CodeFormatButton />
          <InsertLinkButton />
          <TextColorPicker />
          <BackgroundColorPicker />
          <TextFormatDropdown />
          <Divider />
          <InsertDropdown
            enablePoll={true}
            enableEquations={true}
            enableExcalidraw={true}
            enableHorizontalRule={true}
            enableImage={true}
            enableStickyNote={true}
            enableTable={true}
            enableTwitter={true}
            enableYoutube={true}
          />
          <Divider />
          <AlignDropdown />
        </ToolbarPlugin>
      </Editor>
    </EditorComposer>
  );
};

export default VerbumEditor;
