import {
  BottomNavigation,
  BottomNavigationAction,
  Box,
  useTheme
} from '@mui/material';
import HomeIcon from '@mui/icons-material/Home';
import ArticleIcon from '@mui/icons-material/Article';
import { Link as RouterLink } from 'react-router-dom';

const BottomMenu = () => {
  const theme = useTheme();
  return (
    <>
      <Box
        sx={{
          position: 'fixed',
          left: 0,
          right: 0,
          bottom: 0
        }}
      >
        <BottomNavigation
          showLabels
          sx={{
            backgroundColor: theme.palette.pGreen
          }}
        >
          <BottomNavigationAction
            label="Home"
            icon={<HomeIcon />}
            component={RouterLink}
            to={'/'}
          />
          <BottomNavigationAction
            label="Articles"
            icon={<ArticleIcon />}
            component={RouterLink}
            to={'articles'}
          />
        </BottomNavigation>
      </Box>
    </>
  );
};

export default BottomMenu;
