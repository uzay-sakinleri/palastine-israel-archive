import BottomMenu from '@components/BottomMenu';
import Header from '@components/Header';
import { Container, useMediaQuery } from '@mui/material';
import { Outlet } from 'react-router-dom';

const Layout = () => {
  const isSmallScreen = useMediaQuery('(max-width: 425px)');
  return (
    <>
      <Header isSmallScreen={isSmallScreen} />
      <Container
        sx={{
          paddingTop: '100px',
          height: '100%',
          width: '100%'
        }}
      >
        <Outlet />
      </Container>
      {isSmallScreen && <BottomMenu />}
    </>
  );
};

export default Layout;
