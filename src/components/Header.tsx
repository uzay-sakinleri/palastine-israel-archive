import { FC, MouseEvent, PropsWithChildren } from 'react';
import {
  AppBar,
  Menu,
  MenuItem,
  Toolbar,
  Theme,
  IconButton,
  Avatar,
  Drawer,
  List,
  ListItemButton,
  ListItemIcon,
  Box,
  ListItem,
  Button,
  ListItemText
} from '@mui/material';
import HomeIcon from '@mui/icons-material/Home';
import InboxIcon from '@mui/icons-material/Inbox';
import { useState } from 'react';
import { Link as RouterLink, NavLink, Form } from 'react-router-dom';
import { useTheme } from '@emotion/react';
import { useAuthState } from '@hooks/useAuthState.tsx';

export type HeaderProps = {
  isSmallScreen: boolean;
};

const Header: FC<PropsWithChildren<HeaderProps>> = ({ isSmallScreen }) => {
  const [drawerOpen, setDrawerOpen] = useState<boolean>(false);
  const [options, setOptions] = useState<HTMLElement | null>(null);
  const { isAuthenticated } = useAuthState();
  const theme = useTheme() as Theme;
  const handleMenuOpen = (e: MouseEvent<HTMLButtonElement>) => {
    setOptions(e.currentTarget);
  };
  const handleMenuClose = () => {
    setOptions(null);
  };
  const handleDrawer = () => {
    setDrawerOpen((prev) => !prev);
  };

  return (
    <>
      <AppBar>
        <Toolbar
          sx={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            backgroundColor: theme.palette.pBlack,
            height: '70px'
          }}
        >
          <>
            <Button
              onClick={handleDrawer}
              sx={{
                color: theme.palette.pWhite,
                '&:hover': {
                  backgroundColor: theme.palette.pRed
                }
              }}
            >
              Menu
            </Button>
            <nav>
              <Drawer
                anchor="left"
                open={drawerOpen}
                onClose={handleDrawer}
                sx={{ display: 'flex' }}
              >
                <Box
                  width={250}
                  sx={{
                    backgroundColor: theme.palette.pRed,
                    height: '100vh',
                    width: isSmallScreen ? '100px' : 'auto'
                  }}
                >
                  {!isSmallScreen ? (
                    <List className={'nav-links'}>
                      <ListItem>
                        <ListItemButton
                          onClick={handleDrawer}
                          component={NavLink}
                          to={'/'}
                          sx={{
                            color: theme.palette.pBlack,
                            width: '100%'
                          }}
                        >
                          <ListItemIcon>
                            <HomeIcon />
                          </ListItemIcon>
                          <ListItemText>Home</ListItemText>
                        </ListItemButton>
                      </ListItem>
                      <ListItem>
                        <ListItemButton
                          onClick={handleDrawer}
                          sx={{
                            color: theme.palette.pBlack,
                            width: '100%'
                          }}
                          component={NavLink}
                          to={'/articles'}
                        >
                          <ListItemIcon>
                            <InboxIcon />
                          </ListItemIcon>
                          <ListItemText>Articles</ListItemText>
                        </ListItemButton>
                      </ListItem>
                    </List>
                  ) : (
                    <Button
                      onClick={handleDrawer}
                      sx={{
                        color: 'whitesmoke',
                        textTransform: 'none',
                        borderRadius: '0',
                        backgroundColor: theme.palette.pRed + 'FF',
                        '&:active': {
                          backgroundColor: theme.palette.pRed + 'FF'
                        },
                        '&:hover': {
                          backgroundColor: theme.palette.pRed + 'FF'
                        }
                      }}
                      variant="contained"
                      fullWidth
                      disableElevation
                    >
                      Close Flag
                    </Button>
                  )}
                </Box>
                <Box
                  sx={{
                    position: 'fixed',
                    inset: 'auto 0 0 0',
                    backgroundColor: theme.palette.pGreen,
                    height: '70px'
                  }}
                ></Box>
              </Drawer>
            </nav>
          </>
          {isAuthenticated ? (
            <Box>
              <IconButton onClick={handleMenuOpen}>
                <Avatar />
              </IconButton>
              <Menu
                open={options !== null}
                onClose={handleMenuClose}
                anchorEl={options}
              >
                <MenuItem onClick={handleMenuClose}>
                  <Button component={RouterLink} to={'articles/create'}>
                    Create article
                  </Button>
                </MenuItem>
                <MenuItem onClick={handleMenuClose}>
                  <Form action="/logout" method="POST">
                    <Button type="submit">Logout</Button>
                  </Form>
                </MenuItem>
              </Menu>
            </Box>
          ) : (
            <Button
              sx={{
                color: theme.palette.pBlack,
                backgroundColor: theme.palette.pWhite,
                '&:hover': {
                  backgroundColor: theme.palette.pGreen
                }
              }}
              component={RouterLink}
              to={'login'}
            >
              Login
            </Button>
          )}
        </Toolbar>
      </AppBar>
    </>
  );
};

export default Header;
