import { AuthContext } from '@contexts/AuthContext';
import { AuthContextTypes } from '@ptypes/firebaseTypes';
import { useContext } from 'react';

export const useAuthState = () => {
  const authContext = useContext<AuthContextTypes | null>(AuthContext);
  return { ...authContext, isAuthenticated: authContext?.user };
};
