import { app } from './firebase';
import { connectFirestoreEmulator, getFirestore } from 'firebase/firestore';
export const firestore = getFirestore(app);
if (import.meta.env.DEV) connectFirestoreEmulator(firestore, 'localhost', 8080);
