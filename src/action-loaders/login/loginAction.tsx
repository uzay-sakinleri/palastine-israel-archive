import { auth } from '@services/firebase';
import { signInWithEmailAndPassword } from 'firebase/auth';
import { ActionFunction, redirect } from 'react-router-dom';

type Credentials = {
  email: string;
  password: string;
};

const loginAction: ActionFunction = async ({ request }) => {
  try {
    const credentials = Object.fromEntries(
      await request.formData()
    ) as Credentials;
    const res = await signInWithEmailAndPassword(
      auth,
      credentials.email,
      credentials.password
    );
    if (res.user.uid) return redirect('/articles');
    else return redirect('/login');
  } catch (err) {
    return err;
  }
};

export default loginAction;
