import { auth } from '@services/firebase';
import { signOut } from 'firebase/auth';
import { redirect } from 'react-router-dom';

const logoutAction = async () => {
  try {
    await signOut(auth);
    return redirect('/');
  } catch (err) {
    return err as Error;
  }
};

export default logoutAction;
