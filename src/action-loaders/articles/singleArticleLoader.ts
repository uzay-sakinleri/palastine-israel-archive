import { firestore } from '@services/firestore';
import { collection, doc, getDoc } from 'firebase/firestore';
import { LoaderFunction } from 'react-router-dom';

const singleArticleLoader: LoaderFunction = async ({ params }) => {
  try {
    if (params.id) {
      const articles = collection(firestore, 'articles');
      const res = await getDoc(doc(articles, params.id));
      return res.data();
    }
  } catch (err) {
    console.error(err);
    return err;
  }
};

export default singleArticleLoader;
