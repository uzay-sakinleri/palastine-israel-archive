import { firestore } from '@services/firestore';
import { collection, getDocs } from 'firebase/firestore';
import { LoaderFunction } from 'react-router-dom';

const articlesLoader: LoaderFunction = async () => {
  const articles = await getDocs(collection(firestore, 'articles'));
  return { docs: articles.docs };
};

export default articlesLoader;
