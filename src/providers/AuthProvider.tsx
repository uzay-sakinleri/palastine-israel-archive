import { auth } from '@services/firebase';
import { User, onAuthStateChanged } from 'firebase/auth';
import { FC, PropsWithChildren, useEffect, useState } from 'react';
import { AuthContext } from '@contexts/AuthContext';

const AuthProvider: FC<PropsWithChildren> = (props) => {
  const [user, setUser] = useState<User | null>(null);
  const [error, setError] = useState<Error | undefined>(undefined);

  useEffect(() => {
    const unsub = onAuthStateChanged(auth, setUser, setError);
    return unsub;
  }, []);
  return <AuthContext.Provider value={{ user, error, auth }} {...props} />;
};

export default AuthProvider;
