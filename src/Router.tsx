import Layout from '@components/layouts/Layout';
import Login from '@components/Login';
import {
  Link,
  Route,
  createBrowserRouter,
  createRoutesFromElements
} from 'react-router-dom';
import loginAction from './action-loaders/login/loginAction';
import { app } from '@services/firebase';
import { FirebaseTypes } from '@ptypes/firebaseTypes';
import logoutAction from './action-loaders/logout/logoutAction';
import { firestore } from '@services/firestore';
import VerbumEditor from '@components/VerbumEditor';
import Articles from '@routes/Articles';
import articlesLoader from './action-loaders/articles/articlesLoader';
import SingleArticle from '@components/SingleArticle';
import singleArticleLoader from './action-loaders/articles/singleArticleLoader';

const rooterLoader = () => {
  const firebase: FirebaseTypes = { app, firestore };
  return firebase;
};

export const router = createBrowserRouter(
  createRoutesFromElements(
    <>
      <Route
        element={<Layout />}
        id="root"
        errorElement={
          <>
            <h1>Not found</h1>
            <Link to={'..'}>Return to previous</Link>
          </>
        }
        loader={rooterLoader}
      >
        <Route
          index
          element={
            <>
              <h1>Hello</h1>
            </>
          }
        />
        <Route path="articles">
          <Route index element={<Articles />} loader={articlesLoader} />
          <Route
            path=":id"
            element={<SingleArticle />}
            loader={singleArticleLoader}
          />
          <Route path="create" element={<VerbumEditor />} />
        </Route>
        <Route path="login" element={<Login />} action={loginAction} />
        <Route path="logout" action={logoutAction} />
      </Route>
    </>
  )
);
