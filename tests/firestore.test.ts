import {
  RulesTestEnvironment,
  assertFails,
  assertSucceeds,
  initializeTestEnvironment
} from '@firebase/rules-unit-testing';
import { afterAll, beforeAll, describe, test } from 'vitest';
import { readFileSync } from 'fs';
import { getDoc, setDoc } from '@firebase/firestore';
import { Article } from '../src/models/Article';
import { CollectionReference } from '@firebase/firestore-types';
import { loadEnv } from 'vite';
import path from 'path';
import * as uuid from 'uuid';

const env = loadEnv('TEST', path.resolve('.'));

let testEnv: RulesTestEnvironment;
let myUser: ReturnType<typeof testEnv.authenticatedContext>;
let theirUser: ReturnType<typeof testEnv.unauthenticatedContext>;
const userId: string = uuid.v4();

beforeAll(async () => {
  testEnv = await initializeTestEnvironment({
    projectId: env.VITE_PROJECT_ID,
    firestore: {
      rules: readFileSync('firestore.rules', 'utf-8'),
      port: 8080,
      host: '127.0.0.1'
    }
  });
  myUser = testEnv.authenticatedContext(userId);
  theirUser = testEnv.unauthenticatedContext();
  console.log('Authenticated:', myUser);
  console.log('Unauthenticated:', theirUser);
});

afterAll(async () => {
  await testEnv.clearFirestore();
});

describe('Basic public operations', () => {
  let articlesCollection: CollectionReference;
  beforeAll(() => {
    articlesCollection = theirUser.firestore().collection('articles');
  });
  test('Should able to get a single public doc', async () => {
    await assertSucceeds(getDoc(articlesCollection.doc(userId)));
  });

  test("Shouldn't able to set a single doc in articles", async () => {
    await assertFails(setDoc(articlesCollection.doc(userId), {}));
  });
});

describe('Basic authenticated operations', () => {
  let articlesCollection: CollectionReference;
  beforeAll(() => {
    articlesCollection = myUser.firestore().collection('articles');
  });

  test('Should able to write a single doc', async () => {
    const newArticle: Article = {
      id: '1',
      title: 'Hello',
      content: 'World!!!!!!!!!!',
      userId
    };
    await assertSucceeds(setDoc(articlesCollection.doc(userId), newArticle));
  });
  test("Shouldn't able to write to another person's doc", async () => {
    await assertFails(setDoc(articlesCollection.doc(uuid.v4()), {}));
  });
});
